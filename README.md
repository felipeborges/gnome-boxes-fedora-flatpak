To build this, get the flatpak-module-tools copr with

```
sudo dnf copr enable otaylor/flatpak-module-tools-experimental
```

And run

```
sudo dnf update flatpak-module-tools module-build-service
```

To build the module run:

```
flatpak-module build-module --platform=f33
```

(you can pass --rebuild-strategy to it to iterate over the fixes).

And to build the container, run:

```
flatpak-module build-container --from-local --platform=f33
```
